package com.example.mymemory

import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider

class Results : AppCompatActivity() {
    private lateinit var viewModel: InGameViewmodel
    private lateinit var punts: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_results)
        window.decorView.windowInsetsController!!.hide(
            android.view.WindowInsets.Type.statusBars()
        )
        window.decorView.windowInsetsController!!.hide(
            android.view.WindowInsets.Type.navigationBars()
        )

        viewModel = ViewModelProvider(this).get(InGameViewmodel::class.java)
        punts = findViewById(R.id.pointsnum)

        punts.text = viewModel.totalPoints().toString()
    }
}