package com.example.mymemory

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.widget.ImageView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {
    private lateinit var play_button : ImageView
    private lateinit var settings_button : ImageView
    private lateinit var help : ImageView

    @RequiresApi(Build.VERSION_CODES.R)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        window.decorView.windowInsetsController!!.hide(
            android.view.WindowInsets.Type.statusBars()
        )

        play_button = findViewById(R.id.boto_play)
        play_button.setOnClickListener{
            val intent_game = Intent(this, InGame::class.java)
            startActivity(intent_game)
        }

        settings_button = findViewById(R.id.boto_settings)
        settings_button.setOnClickListener{
            val intent_settings = Intent(this,SettingsScreen::class.java)
            startActivity(intent_settings)
        }

        help = findViewById(R.id.info)
        help.setOnClickListener(){
            Toast.makeText(this, "Info of the game: This is a memory game created with android studio By Albert Pons Marques Made with love and affection", Toast.LENGTH_LONG).show()
        }
    }
}