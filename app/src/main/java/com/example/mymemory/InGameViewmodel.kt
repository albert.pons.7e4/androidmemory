package com.example.mymemory

import android.annotation.SuppressLint
import android.widget.TextView
import androidx.lifecycle.ViewModel

@SuppressLint("StaticFieldLeak")
class InGameViewmodel: ViewModel() {

    //contadors
    var tapCounter: Int = 0

    //cartes
    private var imatges = arrayOf(
        R.drawable.planta_svg,
        R.drawable.lampara_svg,
        R.drawable.ic_mobil_svg,
    )
    var cartes = mutableListOf<Carta>()
    var cartesGir = mutableListOf<Carta>()

    private var imatges_x2 = imatges + imatges


    init {
        setDataModel()
    }

    // Funció que barreja les imatges de les cartes i crea l'array de Cartes
    private fun setDataModel() {
        imatges_x2.shuffle()
        for (i in 0..5) {
            this.cartes.add(Carta(i, imatges_x2[i], girada = false, winned = false))
        }
    }


    // Funció que comprova l'estat de la carta, el canvia i envia
    // a la vista la imatge que s'ha de pintar
    fun girarCarta(idCarta: Int) : Int {
        return if (!this.cartes[idCarta].girada) {
            this.cartes[idCarta].girada = true
            this.cartes[idCarta].resId
        } else {
            this.cartes[idCarta].girada = false
            R.drawable.carta_pepit
        }
    }

    // Funció que posa l'estat del joc al mode inicial
    fun resetEstatJoc() {
        for (i in 0..5) {
            if (!cartes[i].winned)
                this.cartes[i].girada = false
        }
    }

    // Funció que retorna l'estat actual d'una carta
    fun estatCarta(idCarta: Int): Int {
        return if(this.cartes[idCarta].girada) this.cartes[idCarta].resId
        else R.drawable.carta_pepit
    }

    fun getTapsText(): String {
        return "$tapCounter"
    }

    fun addTap(v: TextView) {
        v.text = "$tapCounter"
    }

    fun compareCards(c1: Carta, c2: Carta) {
        if ((c1.girada && c2.girada) && (c1.resId == c2.resId)) {
            c1.winned = true
            c2.winned = true
        }
    }

    fun totalPoints(): Int {
        return (1000 - (tapCounter * 50))
    }
}