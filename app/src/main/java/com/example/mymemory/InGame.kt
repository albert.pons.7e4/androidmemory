package com.example.mymemory

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.view.WindowInsets.Type
import android.widget.Chronometer
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider

class InGame : AppCompatActivity(), View.OnClickListener {

    private lateinit var viewModel: InGameViewmodel
    private lateinit var carta1: ImageView
    private lateinit var carta2: ImageView
    private lateinit var carta3: ImageView
    private lateinit var carta4: ImageView
    private lateinit var carta5: ImageView
    private lateinit var carta6: ImageView
    private lateinit var resetButton: ImageView
    private lateinit var crono: Chronometer
    //private lateinit var pauseButton: ImageView
    private lateinit var taps: TextView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_in_game)
        window.decorView.windowInsetsController!!.hide(
            Type.statusBars()
        )

//definició del viewmodel
        viewModel = ViewModelProvider(this).get(InGameViewmodel::class.java)

//variables
        carta1 = findViewById(R.id.carta1)
        carta2 = findViewById(R.id.carta2)
        carta3 = findViewById(R.id.carta3)
        carta4 = findViewById(R.id.carta4)
        carta5 = findViewById(R.id.carta5)
        carta6 = findViewById(R.id.carta6)
        //reset
        resetButton = findViewById(R.id.reset_button)
        //taps
        taps = findViewById(R.id.taps)
        //crono
        crono = findViewById(R.id.chronometerView)

        carta1.setOnClickListener(this)
        carta2.setOnClickListener(this)
        carta3.setOnClickListener(this)
        carta4.setOnClickListener(this)
        carta5.setOnClickListener(this)
        carta6.setOnClickListener(this)
//botó reset
        resetButton.setOnClickListener {
            viewModel.resetEstatJoc()
            updateUI()
            crono.stop()
            crono.start()
        }
        updateUI()

        crono.start()

//cronometre

//        var chronometerPersist = viewModel.cronometro
//        crono = findViewById(R.id.chronometerView)
//
//        val sharedPreferences = getSharedPreferences("ChronometerSample", Context.MODE_PRIVATE)
//        chronometerPersist.getInstance(
//            chronometer = crono,
//            identifier = "mainChronometer",
//            sharedPreferences = sharedPreferences)
//
//        crono.start()

    }
    override fun onClick(v: View?) {
        when (v) {
            carta1 -> {if (viewModel.cartes[0].winned){
                    carta1.isEnabled = false}else
                girarCarta(0, carta1, viewModel.cartes[0])
            }
            carta2 -> {if (viewModel.cartes[1].winned){
                carta2.isEnabled = false}else
                girarCarta(1, carta2, viewModel.cartes[1])
            }
            carta3 -> {if (viewModel.cartes[2].winned){
                carta3.isEnabled = false}else
                girarCarta(2, carta3, viewModel.cartes[2])
            }
            carta4 -> {if (viewModel.cartes[3].winned){
                carta4.isEnabled = false}else
                girarCarta(3, carta4, viewModel.cartes[3])
            }
            carta5 -> {if (viewModel.cartes[4].winned){
                carta5.isEnabled = false}else
                girarCarta(4, carta5, viewModel.cartes[4])
            }
            carta6 -> {if (viewModel.cartes[5].winned){
                carta6.isEnabled = false}else
                girarCarta(5, carta6, viewModel.cartes[5])
            }
        }
        addTap(taps)
        isFinished()
    }

    // Funció que utilitzarem per girar la carta
    private fun girarCarta(idCarta: Int, carta: ImageView, c: Carta) {
        carta.setImageResource(viewModel.girarCarta(idCarta))
        viewModel.cartesGir.add(c)
        if (viewModel.cartesGir.size == 2) {
            viewModel.compareCards(viewModel.cartesGir[0], viewModel.cartesGir[1])
            viewModel.cartesGir.clear()
            Handler().postDelayed({
                viewModel.resetEstatJoc()
                updateUI()
            }, 400)

        }
    }

    // Funció que restauarà l'estat de la UI
    private fun updateUI() {
        carta1.setImageResource(viewModel.estatCarta(0))
        carta2.setImageResource(viewModel.estatCarta(1))
        carta3.setImageResource(viewModel.estatCarta(2))
        carta4.setImageResource(viewModel.estatCarta(3))
        carta5.setImageResource(viewModel.estatCarta(4))
        carta6.setImageResource(viewModel.estatCarta(5))
        taps.text = viewModel.getTapsText()
    }

    private fun addTap(v: TextView) {
        viewModel.tapCounter++
        viewModel.addTap(v)
    }

    private fun acabat(){
        val intent_results = Intent(this, Results::class.java)
        startActivity(intent_results)
    }

    private fun isFinished() {
        var num = 0
        for (i in 0..5){
            if (viewModel.cartes[i].winned){
                num++
            }
        }
        if (num == 6){
            acabat()
        }
    }
}